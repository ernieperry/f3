﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPressureModel : MonoBehaviour {

    private GameObject pressureModel;

    // Use this for initialization
    void Start()
    {
        pressureModel = GameObject.Find("pmean");
        pressureModel.SetActive(true);
    }

    public void Show()
    {
        pressureModel.SetActive(true);
    }

    public void Hide()
    {
        pressureModel.SetActive(false);
    }
}
