﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeTransformChildren : MonoBehaviour {

    [SerializeField]
    Slider heightSlider;
    [SerializeField]
    Slider rotationSlider;
    private GameObject parentObj;
    

    private void Start()
    {
        if (heightSlider)
        {
            heightSlider.onValueChanged.AddListener(UpdateHeight);
        }

        if (rotationSlider)
        {
            rotationSlider.onValueChanged.AddListener(UpdateRotation);
        }

        parentObj = this.gameObject;
    }

    public void UpdateHeight(float value)
    {
        foreach (Transform child in parentObj.transform)
        {
            transform.position = new Vector3(0.0f, 0.0f + value / 100.0f, 0.0f);
        }
    }

    public void UpdateRotation(float value)
    {
        foreach (Transform child in parentObj.transform)
        {
            transform.eulerAngles = new Vector3(value, 0.0f, 0.0f);
        }
    }
}
