﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//public class ElevationSlider : MonoBehaviour,  IPointerDownHandler {
//public class SliderManager : MonoBehaviour, ISelectHandler
public class SliderManager : MonoBehaviour
{
    [SerializeField] GameObject controllerObj;

    private SteamVR_TrackedObject trackedObject;
    private SteamVR_TrackedController controller;
    private SteamVR_Controller.Device device;

    private GameObject transformationMenu;
    private GameObject elevationSliderObj;
    private GameObject rotationSliderObj;
    private Slider elevationSlider;
    private Slider rotationSlider;
    private bool elevationSliderSelected = true;

    private readonly Vector2 mXAxis = new Vector2(1, 0);
    private readonly Vector2 mYAxis = new Vector2(0, 1);
    private bool trackingSwipe = false;
 	private bool checkSwipe = false;

    // The angle range for detecting swipe
    private const float mAngleRange = 30;

    private Vector2 mStartPosition;
    private Vector2 endPosition;
    private Vector2 swipeVector;

    private int elevationRange;
    private int rotationRange;
    private float increment;
    private float angleOfSwipe;

    // Use this for initialization
    void Start ()
    {
        trackedObject = controllerObj.GetComponent<SteamVR_TrackedObject>();
        controller = controllerObj.GetComponent<SteamVR_TrackedController>();
        controller. PadClicked += ManagePadClick;

        transformationMenu = this.gameObject;
        elevationSliderObj = transformationMenu.transform.Find("Slider Elevation").gameObject;
        elevationSlider = elevationSliderObj.gameObject.GetComponent<Slider>();

        rotationSliderObj = transformationMenu.transform.Find("Slider Rotation").gameObject;
        rotationSlider = rotationSliderObj.gameObject.GetComponent<Slider>();

        elevationRange = (int)(elevationSlider.maxValue - elevationSlider.minValue);
        rotationRange = (int)(rotationSlider.maxValue - rotationSlider.minValue);
    }

    void Update()
    {
        device = SteamVR_Controller.Input((int)trackedObject.index);
   
        // Touchpad down
        if (device.GetTouchDown(Valve.VR.EVRButtonId.k_EButton_Axis0))
        {
            trackingSwipe = true;
            checkSwipe = true;
            // Record position
            mStartPosition = new Vector2(device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0).x, device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0).y);
            endPosition = mStartPosition;
        }

        // Touchpad up            
        else if (device.GetTouchUp(Valve.VR.EVRButtonId.k_EButton_Axis0))
        {
            trackingSwipe = false;
            checkSwipe = false;
        }
            
        else if (trackingSwipe)
        {
            endPosition = new Vector2(device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0).x, device.GetAxis(Valve.VR.EVRButtonId.k_EButton_Axis0).y);
        }

        if (checkSwipe)
        {
            if (elevationSliderSelected)
            {
                swipeVector = endPosition - mStartPosition;

                increment = swipeVector.magnitude / 2.0f;
                swipeVector.Normalize();

                angleOfSwipe = Vector2.Dot(swipeVector, mXAxis);
                angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;

                // Detect left and right swipe
                if (angleOfSwipe < mAngleRange)
                {
                    OnSwipeRight();
                }
                else if ((180.0f - angleOfSwipe) < mAngleRange)
                {
                    OnSwipeLeft();
                }
                else
                {
                    // Detect top and bottom swipe
                    angleOfSwipe = Vector2.Dot(swipeVector, mYAxis);
                    angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;
                    if (angleOfSwipe < mAngleRange)
                    {
                        OnSwipeTop();
                    }
                    else if ((180.0f - angleOfSwipe) < mAngleRange)
                    {
                        OnSwipeBottom();
                    }
                }
                mStartPosition = endPosition;
            }
            else
            {
                bool rotateCW;
                if (endPosition.x > mStartPosition.x)
                    rotateCW = true;
                else
                    rotateCW = false;               

                float dotProduct = Vector2.Dot(endPosition, mStartPosition);
                float cosAngle = dotProduct / (endPosition.magnitude * mStartPosition.magnitude);
                if (cosAngle >= 1.0f)
                    return;

                angleOfSwipe = Mathf.Acos(cosAngle) * Mathf.Rad2Deg;

                if (rotateCW)
                {
                    OnSwipeCW();
                }
                else
                {
                    OnSwipeCCW();
                }

                mStartPosition = endPosition;
            }
        }
    } 
    /*
    void ISelectHandler.OnSelect(BaseEventData eventData)
    {
        
        string name1 = sliderObj.name;
        string name2 = eventData.selectedObject.name;

        if (eventData.selectedObject != sliderObj)
            sliderSelected = false;
        else
            sliderSelected = true;
       
    }
    */
    void ManagePadClick(object sender, ClickedEventArgs e)
    {
        if (elevationSliderSelected)
        {
            elevationSliderSelected = false;
        }
        else
        {
            elevationSliderSelected = true;
        }
    }

    private void OnSwipeLeft()
    {
        elevationSlider.value -= (increment * elevationRange);
    }

    private void OnSwipeRight()
    {
        elevationSlider.value += (increment * elevationRange);
    }

    private void OnSwipeTop()
    {
        //mMessageIndex = 3;
    }

    private void OnSwipeBottom()
    {
        //mMessageIndex = 4;
    }

    private void OnSwipeCW()
    {
        rotationSlider.value += angleOfSwipe;
        if (rotationSlider.value > 90.0f)
            rotationSlider.value = 90.0f;
    }

    private void OnSwipeCCW()
    {
        rotationSlider.value -= angleOfSwipe;
        if (rotationSlider.value < -90.0f)
            rotationSlider.value = -90.0f;
    }
}
