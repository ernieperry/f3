﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeHeight2 : MonoBehaviour {

    private Slider slider;
    private GameObject obj;
    private static GameObject sliderObj = null;

    private void Awake()
    {
        if (sliderObj == null)
            sliderObj = GameObject.FindGameObjectWithTag("slider_height");
    }

    private void Start()
    {
        if (sliderObj)
        {
            slider = sliderObj.GetComponent<Slider>();
            slider.onValueChanged.AddListener(UpdateHeight);
        }
        obj = this.gameObject;
    }

    public void UpdateHeight(float value)
    {
        transform.position = new Vector3(0.0f, 0.0f + value / 100.0f, 0.0f);
    }
}
