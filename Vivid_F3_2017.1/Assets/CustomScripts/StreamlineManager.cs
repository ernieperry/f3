﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreamlineManager : MonoBehaviour
{

    private List<GameObject> leftStreamlines = new List<GameObject>(6);
    private List<GameObject> rightStreamlines = new List<GameObject>(6);

    // Use this for initialization
    void Start ()
    {
        leftStreamlines.Add(GameObject.Find("/vtkStreamlines/Left/left_streamlines-045"));
        leftStreamlines.Add(GameObject.Find("/vtkStreamlines/Left/left_streamlines-145"));
        leftStreamlines.Add(GameObject.Find("/vtkStreamlines/Left/left_streamlines-245"));
        leftStreamlines.Add(GameObject.Find("/vtkStreamlines/Left/left_streamlines-345"));
        leftStreamlines.Add(GameObject.Find("/vtkStreamlines/Left/left_streamlines-445"));
        leftStreamlines.Add(GameObject.Find("/vtkStreamlines/Left/left_streamlines-545"));

        rightStreamlines.Add(GameObject.Find("/vtkStreamlines/Right/right_streamlines-045"));
        rightStreamlines.Add(GameObject.Find("/vtkStreamlines/Right/right_streamlines-145"));
        rightStreamlines.Add(GameObject.Find("/vtkStreamlines/Right/right_streamlines-245"));
        rightStreamlines.Add(GameObject.Find("/vtkStreamlines/Right/right_streamlines-345"));
        rightStreamlines.Add(GameObject.Find("/vtkStreamlines/Right/right_streamlines-445"));
        rightStreamlines.Add(GameObject.Find("/vtkStreamlines/Right/right_streamlines-545"));

        SetInactive();
    }

    public void UpdateDisplay(int value)
    {
        SetInactive();

        if (value == 0)
        {
            return;
        }

        leftStreamlines[value-1].SetActive(true);
        rightStreamlines[value-1].SetActive(true);
    }

    public void SetInactive()
    {
        for (int i=0; i<leftStreamlines.Count; i++)
        {
            leftStreamlines[i].SetActive(false);
        }

        for (int i = 0; i < rightStreamlines.Count; i++)
        {
            rightStreamlines[i].SetActive(false);
        }
    }

    public void SetActive()
    {
        for (int i = 0; i < leftStreamlines.Count; i++)
        {
            leftStreamlines[i].SetActive(true);
        }

        for (int i = 0; i < rightStreamlines.Count; i++)
        {
            rightStreamlines[i].SetActive(true);
        }
    }
}
