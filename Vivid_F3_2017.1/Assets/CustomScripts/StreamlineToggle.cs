﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StreamlineToggle : MonoBehaviour {

    public StreamlineManager streamManager;
    public GameObject streamlineSlider;
    private Toggle streamToggle;

    public void Start()
    {
        streamToggle = this.gameObject.GetComponent<Toggle>();

        //Adds a listener to the main slider and invokes a method when the value changes.
        streamToggle.onValueChanged.AddListener((state) => Toggled(state));
    }

    // Update is called once per frame
	void Toggled(bool state)
    {
        //bool toggleOn = this.gameObject.GetComponent<Toggle>().isOn;
        if (state)
        {
            streamlineSlider.GetComponent<Slider>().enabled = false;
            streamManager.SetActive();
        }
        else
        {
            streamlineSlider.GetComponent<Slider>().enabled = true;
            streamlineSlider.GetComponent<Slider>().value = 0.0f;
            streamManager.SetInactive();
        }
    }
}
