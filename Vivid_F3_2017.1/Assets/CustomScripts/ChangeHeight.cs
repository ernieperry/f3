﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeHeight : MonoBehaviour {

    //private static Transform F3Transform;

    private void Awake()
    {
        //F3Transform = GetComponent<Transform>();
    }

    public void UpdateHeight(float value)
    {
        //transform.position = new Vector3(F3Transform.position.x, F3Transform.position.y + value, F3Transform.position.z);
        transform.position = new Vector3(0.0f, 0.0f + value/100.0f, 0.0f);
    }
}
