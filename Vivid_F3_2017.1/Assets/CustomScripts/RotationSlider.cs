﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RotationSlider : MonoBehaviour, ISelectHandler
{
    private GameObject sliderObj = null;
    private Slider rotationSlider = null;
    private bool sliderSelected = false;

    // Use this for initialization
    void Start ()
    {
        sliderObj = this.gameObject;
        rotationSlider = this.gameObject.GetComponent<Slider>();
        rotationSlider.SendMessage("Deselect");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void ISelectHandler.OnSelect(BaseEventData eventData)
    {
        string name1 = sliderObj.name;
        string name2 = eventData.selectedObject.name;

        if (eventData.selectedObject != sliderObj)
            sliderSelected = false;
        else
            sliderSelected = true;
    }
}
