﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    private SteamVR_TrackedController device;
    private GameObject menuObj;
    private Canvas menuCanvas;
    private GameObject hmdObj;
    private static List<GameObject> child = new List<GameObject>();
    private static bool listInitialized = false;

    // Use this for initialization
    
    public void Awake ()
    {
        if (!listInitialized)
        {
            // Get all of the menus
            int nbrChildren = this.gameObject.transform.childCount;
            for (int i = 0; i < nbrChildren; i++)
            {
                GameObject childObj = this.gameObject.transform.GetChild(i).gameObject;
                child.Add(childObj);
            }
            
            listInitialized = true;
        }
    }
    
    public void Start()
    {
        device = GetComponent<SteamVR_TrackedController>();
        if (device != null)
            device.MenuButtonClicked += ManageMenu;

        hmdObj = GameObject.FindGameObjectWithTag("MainCamera");

        for (int i = 0; i < child.Count; i++)
        {
            if (child[i].name == "MainMenu")
            {
                menuObj = child[i];
                menuObj.SetActive(true);
            }
            else
            {
                child[i].SetActive(false);
            }
        }

        if (menuObj != null)
        {
            menuCanvas = menuObj.transform.parent.GetComponent<Canvas>();
            if (menuCanvas != null)
                menuCanvas.enabled = false;
        }
    }
	
	// Turns the menu off and on
	void ManageMenu (object sender, ClickedEventArgs e)
    {
		if (menuObj != null)
        {
            if (menuCanvas.GetComponent<Canvas>().enabled == true)
            {
                menuCanvas.GetComponent<Canvas>().enabled = false;
            }
            else
            {
                Vector3 menuOrigin = Vector3.zero;
                if (hmdObj != null)
                {
                    Ray raydirection = new Ray(hmdObj.transform.position, hmdObj.transform.forward);
                    menuOrigin = raydirection.GetPoint(1.5f);
                    menuCanvas.transform.position = menuOrigin;
                    menuCanvas.transform.forward = hmdObj.transform.forward;
                }
                
                menuCanvas.GetComponent<Canvas>().enabled = true;
            }
        }
	}

    //void SetMenuPosition(object sender, ClickedEventArgs e)
    void SetMenuPosition()
    {
        if (menuObj != null)
        {
            Vector3 menuOrigin;
            if (device != null)
            {
                Ray raydirection = new Ray(device.transform.position, device.transform.forward);
                menuOrigin = raydirection.GetPoint(1.4f);
                menuCanvas.transform.position = menuOrigin;
                menuCanvas.transform.forward = device.transform.forward;
            }
        }
    }
    
    private void Update()
    {
        if (device == null)
            return;

        if (device.gripped)
        {
            SetMenuPosition();
        }
    } 
    
}
