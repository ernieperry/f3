﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeTransform : MonoBehaviour {

    [SerializeField] Slider heightSlider;
    [SerializeField] Slider rotationSlider;
    //private GameObject obj;
    //private static GameObject sliderObj = null;

    private void Start()
    {
        if (heightSlider)
        {
            //slider = sliderObj.GetComponent<Slider>();
            heightSlider.onValueChanged.AddListener(UpdateHeight);
        }

        if (rotationSlider)
        {
            //slider = sliderObj.GetComponent<Slider>();
            rotationSlider.onValueChanged.AddListener(UpdateRotation);
        }
    }

    public void UpdateHeight(float value)
    {
        transform.position = new Vector3(0.0f, 0.0f + value / 100.0f, 0.0f);
    }

    public void UpdateRotation(float value)
    {
        transform.eulerAngles = new Vector3(value, 0.0f, 0.0f);
    }
}
