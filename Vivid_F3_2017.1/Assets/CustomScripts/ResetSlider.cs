﻿using UnityEngine;
using UnityEngine.UI;

public class ResetSlider : MonoBehaviour {

    private Slider slider;

    void Start()
    {
        slider = GetComponent<Slider>();
    }

    public void Reset()
    {  
        slider.value = 0.0f;
    }
}
