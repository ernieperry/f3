﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeRotation : MonoBehaviour {

    //private static Transform F3Transform;

    private void Awake()
    {
        //F3Transform = GetComponent<Transform>();
    }

    public void UpdateRotation(float value)
    {
        transform.eulerAngles = new Vector3(value, 0.0f, 0.0f);
    }
}
