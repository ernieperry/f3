﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StreamlineSlider : MonoBehaviour {

    public StreamlineManager manager;
    private Slider streamSlider;

    public void Start()
    {
        streamSlider = this.gameObject.GetComponent<Slider>();

        //Adds a listener to the main slider and invokes a method when the value changes.
        streamSlider.onValueChanged.AddListener(delegate { SliderMoved(); });
    }
    // Update is called once per frame
	public void SliderMoved ()
    {
        int position = (int)this.gameObject.GetComponent<Slider>().value;

        manager.UpdateDisplay(position);
    }
}
