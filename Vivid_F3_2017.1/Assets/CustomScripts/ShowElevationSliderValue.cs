﻿using System.Reflection.Emit;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ShowElevationSliderValue : MonoBehaviour {

    // shows the change in elevation in meters
    public void UpdateLabel(float value)
    {
        Text label = GetComponent<Text>();
        if (label != null)
            label.text = Mathf.RoundToInt(value) + " cm";
    }
}
