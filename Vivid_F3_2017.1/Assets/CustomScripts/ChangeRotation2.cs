﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeRotation2 : MonoBehaviour {

    private Slider slider;
    private GameObject obj;
    private static GameObject sliderObj = null;

    private void Awake()
    {
        if (sliderObj == null)
            sliderObj = GameObject.FindGameObjectWithTag("slider_rotation");
    }

    private void Start()
    {
        if (sliderObj)
        {
            slider = sliderObj.GetComponent<Slider>();
            slider.onValueChanged.AddListener(UpdateRotation);
        }
        obj = this.gameObject;
    }

    public void UpdateRotation(float value)
    {
        transform.eulerAngles = new Vector3(value, 0.0f, 0.0f);
    }
}
