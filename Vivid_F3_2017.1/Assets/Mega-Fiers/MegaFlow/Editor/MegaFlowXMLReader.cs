﻿
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MegaFlowXMLValue
{
	public string	name;
    //public string	value;
    public List<string> data;

    public MegaFlowXMLValue()
    {
        data = new List<string>();
    }
}

public class MegaFlowXMLNode
{
    public int                      inData;
	public String					tagName;
	public MegaFlowXMLNode			parentNode;
	public List<MegaFlowXMLNode>	children;
	public List<MegaFlowXMLValue>	values;

	public MegaFlowXMLNode()
	{
        inData = 0;
		tagName = "NONE";
		parentNode = null;
		children = new List<MegaFlowXMLNode>();
		values = new List<MegaFlowXMLValue>();
	}
}

public class MegaFlowXMLReader
{
	private static char TAG_START = '<';
	private static char TAG_END = '>';
	private static char SPACE = ' ';
	private static char QUOTE = '"';
	private static char SLASH = '/';
	private static char EQUALS = '=';
    private static char SEMICOLON = ';';
    private static String BEGIN_QUOTE = "" + EQUALS + QUOTE;

	public MegaFlowXMLReader()
	{
	}

    public MegaFlowXMLNode createNode()
    {
        return (new MegaFlowXMLNode());
    }

    public MegaFlowXMLNode read(MegaFlowXMLNode rootNode, String xml)
	{
		int index = 0;
		int lastIndex = 0;
		//MegaFlowXMLNode rootNode = new MegaFlowXMLNode();
		MegaFlowXMLNode currentNode = rootNode;

		//xml = xml.Replace(" \n", "");
		//xml = xml.Replace("\n", "");

		while ( true )
		{
            // this section is for when we are reading the velocity values line by line
            if (currentNode.inData == 2)
            {
                lastIndex = xml.IndexOf(TAG_END, index);
                if (lastIndex == -1) // if lastIndex is equal to -1 we are not to the end yet
                {
                    // get rid of the semicolon
                    String xmltmp = xml.Substring(0, xml.Length-1);
                    
                    int nbrvals = currentNode.values.Count;
                    MegaFlowXMLValue value = currentNode.values[0];
                    value.data.Add(xmltmp);

                    return currentNode;
                }
                else
                {
                    // if we are here, we are at the end of the file
                    currentNode.inData = 0;
                    currentNode.parentNode.inData = 0;
                    return currentNode.parentNode;
                }
            }

            // this finds the start of a data section
			index = xml.IndexOf(TAG_START, lastIndex);

			if ( index < 0 || index >= xml.Length )
				break;

			index++;

            // this finds the end of a data section
			lastIndex = xml.IndexOf(TAG_END, index);
            // check to see if we are in the velocity data
            if (lastIndex == -1)
            {
                lastIndex = xml.IndexOf(SEMICOLON, index);
                currentNode.inData = 1;
            }
			if ( lastIndex < 0 || lastIndex >= xml.Length )
				break;

			int tagLength = lastIndex - index;
			String xmlTag = xml.Substring(index, tagLength);

			if ( xmlTag[0] == SLASH )
			{
				currentNode = currentNode.parentNode;
				continue;
			}

			bool openTag = true;

			if ( xmlTag[tagLength - 1] == SLASH )
			{
				xmlTag = xmlTag.Substring(0, tagLength - 1);
				openTag = false;
			}

            MegaFlowXMLNode node = new MegaFlowXMLNode();
            if (currentNode.inData == 1)
            {
                node.inData = 1;
            }
            parseTag(node, xmlTag);
            node.parentNode = currentNode;
            /*
            if (currentNode.inData == 1)
            {
                currentNode.children.Add(node);
                node.parentNode = currentNode;
            }
            */
            currentNode.children.Add(node);

            if ( openTag )
				currentNode = node;

            if (currentNode.inData != 0)
                return currentNode;
		}

		//return rootNode;
        return currentNode;
    }

	public MegaFlowXMLNode parseTag(MegaFlowXMLNode node, String xmlTag)
	{
		//MegaFlowXMLNode node = new MegaFlowXMLNode();

		int nameEnd = xmlTag.IndexOf(SPACE, 0);
		if ( nameEnd < 0 )
		{
			node.tagName = xmlTag;
			return node;
		}

		String tagName = xmlTag.Substring(0, nameEnd);
		node.tagName = tagName;

		String attrString = xmlTag.Substring(nameEnd, xmlTag.Length - nameEnd);
		return parseAttributes(attrString, node);
	}

	public MegaFlowXMLNode parseAttributes(String xmlTag, MegaFlowXMLNode node)
	{
		int index = 0;
		int attrNameIndex = 0;
		int lastIndex = 0;

        while (true)
        {
            index = xmlTag.IndexOf(BEGIN_QUOTE, lastIndex);
            if (index < 0 || index > xmlTag.Length)
                break;

            attrNameIndex = xmlTag.LastIndexOf(SPACE, index);
            if (attrNameIndex < 0 || attrNameIndex > xmlTag.Length)
                break;

            attrNameIndex++;
            String attrName = xmlTag.Substring(attrNameIndex, index - attrNameIndex);

            index += 2;

            if (node.inData == 1)
            {
                lastIndex = xmlTag.Length;
                node.inData = 2;
            }
            else
            {
                lastIndex = xmlTag.IndexOf(QUOTE, index);
                if (lastIndex < 0 || lastIndex > xmlTag.Length)
                {
                    break;
                }
            }


			int tagLength = lastIndex - index;
			String attrValue = xmlTag.Substring(index, tagLength);

			MegaFlowXMLValue val = new MegaFlowXMLValue();
			val.name = attrName;
			val.data.Add(attrValue);
			node.values.Add(val);

            if (node.inData == 2)
            {
                return node;
            }
        }

		return node;
	}

	static char[] commaspace = new char[] { ',', ' ' };

	static public Vector3 ParseV3Split(string str, int i)
	{
		return ParseV3(str.Split(commaspace, StringSplitOptions.RemoveEmptyEntries), i);
	}

	static public Vector3 ParseV3(string[] str, int i)
	{
		Vector3 p = Vector3.zero;

		p.x = float.Parse(str[i]);
		p.y = float.Parse(str[i + 1]);
		p.z = float.Parse(str[i + 2]);
		return p;
	}
}
