
using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using IVI_CFD;

public class MegaFlowFLW
{
	public static ImageDataSet LoadFrame(string filename)
	{
		ImageDataSet flow = null;

		if ( File.Exists(filename) )
		{
			flow = ScriptableObject.CreateInstance<ImageDataSet>();
			flow.Init();
			LoadFLW(flow, filename);
		}

		return flow;
	}

	public static ImageDataSet LoadFrame(string filename, int frame, string namesplit)
	{
		ImageDataSet flow = null;

		char[]	splits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

		string dir= Path.GetDirectoryName(filename);
		string file = Path.GetFileNameWithoutExtension(filename);

		string[] names;

		if ( namesplit.Length > 0 )
		{
			names = file.Split(namesplit[0]);
			names[0] += namesplit[0];
		}
		else
			names = file.Split(splits);

		if ( names.Length > 0 )
		{
			string newfname = dir + "/" + names[0] + frame + ".flw";
			flow = LoadFrame(newfname);
		}

		return flow;
	}

#if false
	public static MegaFlowFrame LoadFrame(string filename, int frame)
	{
		MegaFlowFrame flow = null;

		char[]	splits = { '-' };

		string fname = filename; // use unity get path

		string[] names = fname.Split(splits);

		if ( names.Length > 0 )
		{
			string newfname = names[0] + "-" + frame + ".flw";
			flow = LoadFrame(newfname);
		}

		return flow;
	}
#endif

	static public void LoadFLW(ImageDataSet flow, string filename)
	{
        /*
		StreamReader streamReader = new StreamReader(filename);
		string data = streamReader.ReadToEnd();
		streamReader.Close();

		MegaFlowXMLReader xml = new MegaFlowXMLReader();
		MegaFlowXMLNode node = xml.read(data);
        */
        MegaFlowXMLReader xml = new MegaFlowXMLReader();
        MegaFlowXMLNode node = xml.createNode();

        using (StreamReader sr = new StreamReader(filename))
        {
            while (sr.Peek() >= 0)
            {
                string data = sr.ReadLine();
                node = xml.read(node, data);
            }
        }
        
        //ParseXML1(flow, node);
        ParseXML1Unity(flow, node);

        xml = null;
		//data = null;
		GC.Collect();
	}

	static Vector3 ReadV3(string[] vals)
	{
		Vector3 v = Vector3.zero;

		v.x = float.Parse(vals[index++]);
		v.y = float.Parse(vals[index++]);
		v.z = float.Parse(vals[index++]);

        return v;
	}

    static Vector3 ReadV3Unity(string[] vals)
    {
        Vector3 v = Vector3.zero;

        v.x = float.Parse(vals[0]);
        v.y = float.Parse(vals[1]);
        v.z = float.Parse(vals[2]);

        return v;
    }

    static Vector3 ReadV3Adj(string[] vals)
	{
		Vector3 v = ReadV3(vals);
        
		//v.z = -v.z;
        v.y = -v.y;

        return v;
	}

    // Vivid added function 8.22.17 EP
    static Vector3 ReadV3AdjUnity(string[] vals)
    {
        // we read the velocities and then rotate them so that they are now Y-up instead of Z-up (from vtk)
        Vector3 v = ReadV3Unity(vals);
    
        //v.z = -v.z;

        //return v;

        // this code gives the correct orientation for the F3 model EP
        Vector3 v2 = new Vector3(v.x, v.z, v.y);

        return v2;
    }

    static int index = 0;

	static public void ParseXML1(ImageDataSet flow, MegaFlowXMLNode node)
	{
		foreach ( MegaFlowXMLNode n in node.children )
		{
			switch ( n.tagName )
			{
				case "Fluid": ParseFluid(flow, n); break;
				default: Debug.Log("Unknown Fluid Node " + n.tagName); break;
			}
		}
	}

    static public void ParseXML1Unity(ImageDataSet flow, MegaFlowXMLNode node)
    {
        foreach (MegaFlowXMLNode n in node.children)
        {
            switch (n.tagName)
            {
                case "Fluid": ParseFluidUnity(flow, n); break;
                default: Debug.Log("Unknown Fluid Node " + n.tagName); break;
            }
        }
    }

    static void ParseFluid(ImageDataSet flow, MegaFlowXMLNode node)
	{
		for ( int i = 0; i < node.values.Count; i++ )
		{
			MegaFlowXMLValue val = node.values[i];

			switch ( val.name )
			{
				case "grid":
					{
						string[] vals = val.data[0].Split(',');
						flow.gridDim2[0] = int.Parse(vals[0]);
						flow.gridDim2[1] = int.Parse(vals[1]);
						flow.gridDim2[2] = int.Parse(vals[2]);
						break;
					}

				case "size":
					{
						string[] vals = val.data[0].Split(',');
						index = 0;
						Vector3 bmin = ReadV3(vals);
						Vector3 bmax = ReadV3(vals);

						flow.size = bmax - bmin;
						flow.gsize = flow.size;

						// griddim should have a name change
						flow.spacing.x = flow.size.x / flow.gridDim2[0];
						flow.spacing.y = flow.size.y / flow.gridDim2[1];
						flow.spacing.z = flow.size.z / flow.gridDim2[2];
						flow.oos.x = 1.0f / flow.spacing.x;
						flow.oos.y = 1.0f / flow.spacing.y;
						flow.oos.z = 1.0f / flow.spacing.z;

                        flow.origin = bmin;
                        break;
					}

				default: Debug.Log("Unknown Fluid attribute " + val.name); break;
			}
		}

		for ( int i = 0; i < node.children.Count; i++ )
		{
			MegaFlowXMLNode n = (MegaFlowXMLNode)node.children[i];
			switch ( n.tagName )
			{
				case "Vel":
					ParseVel(flow, n);
					break;

				case "Force":
					break;

				case "Density":
					break;

				default: Debug.Log("Unknown Fluid node " + n.tagName); break;
			}
		}
	}

    static void ParseFluidUnity(ImageDataSet flow, MegaFlowXMLNode node)
    {
        for (int i = 0; i < node.values.Count; i++)
        {
            MegaFlowXMLValue val = node.values[i];

            switch (val.name)
            {
                case "grid":
                    {
                        // set the dimensions correctly to read the vtk data
                        string[] vals = val.data[0].Split(',');
                        flow.gridDim2[0] = int.Parse(vals[0]);
                        flow.gridDim2[1] = int.Parse(vals[1]);
                        flow.gridDim2[2] = int.Parse(vals[2]);
                        break;
                    }

                case "size":
                    {
                        // set the data correctly to read the vtk data
                        string[] vals = val.data[0].Split(',');
                        index = 0;
                        Vector3 bmin = ReadV3(vals);
                        Vector3 bmax = ReadV3(vals);

                        flow.size = bmax - bmin;
                        flow.gsize = flow.size;

                        // griddim should have a name change
                        flow.spacing.x = flow.size.x / flow.gridDim2[0];
                        flow.spacing.y = flow.size.y / flow.gridDim2[1];
                        flow.spacing.z = flow.size.z / flow.gridDim2[2];
                        flow.oos.x = 1.0f / flow.spacing.x;
                        flow.oos.y = 1.0f / flow.spacing.y;
                        flow.oos.z = 1.0f / flow.spacing.z;

                        flow.origin = bmin;
                        break;
                    }

                default: Debug.Log("Unknown Fluid attribute " + val.name); break;
            }
        }

        for (int i = 0; i < node.children.Count; i++)
        {
            //NOTE: this transform now only works for 1 node

            MegaFlowXMLNode n = (MegaFlowXMLNode)node.children[i];
            switch (n.tagName)
            {
                case "Vel":
                    // When we read in the velocity data we have to rotate it to Y-up left hand system from Z-up right hand system
                    ParseVelUnity(flow, n);

                    // After the velocity vectors have been transformed we have to transform the image data bounding box 
                    // to match the lefthand, Y up convention of Unity
                    int xDim = flow.gridDim2[0];
                    int yDim = flow.gridDim2[2];
                    int zDim = flow.gridDim2[1];
                    flow.gridDim2[0] = xDim;
                    flow.gridDim2[1] = yDim;
                    flow.gridDim2[2] = zDim;

                    Vector3 origin = flow.origin;
                    Vector3 size = flow.size;
                    flow.origin.Set(origin.x, origin.z, origin.y);
                    flow.size.Set(size.x, size.z, size.y);
                    flow.gsize = flow.size;

                    flow.spacing.x = flow.size.x / flow.gridDim2[0];
                    flow.spacing.y = flow.size.y / flow.gridDim2[1];
                    flow.spacing.z = flow.size.z / flow.gridDim2[2];
                    flow.oos.x = 1.0f / flow.spacing.x;
                    flow.oos.y = 1.0f / flow.spacing.y;
                    flow.oos.z = 1.0f / flow.spacing.z;

                    break;

                case "Force":
                    break;

                case "Density":
                    break;

                default: Debug.Log("Unknown Fluid node " + n.tagName); break;
            }
        }
    }

    static void ParseVel(ImageDataSet flow, MegaFlowXMLNode node)
	{
        int cnt = node.values.Count;

		for ( int i = 0; i < node.values.Count; i++ )
		{
			MegaFlowXMLValue val = node.values[i];

			switch ( val.name )
			{
				case "data":
					{
						//string[] vals = val.value.Split(',');
                        index = 0;

                        //int len = vals.Length / 3;

                        flow.vel.Clear();

                        //int numbrpts = flow.gridDim2[0] * flow.gridDim2[1] * flow.gridDim2[2];
                        Vector3[] vels = new Vector3[flow.gridDim2[0] * flow.gridDim2[1] * flow.gridDim2[2]];

						for ( int z = 0; z < flow.gridDim2[2]; z++ )
						{
							for ( int y = 0; y < flow.gridDim2[1]; y++ )
							{
                                for (int x = 0; x < flow.gridDim2[0]; x++)
                                {
                                    int indx = x + (y * flow.gridDim2[0]) + (z * flow.gridDim2[1] * flow.gridDim2[2]);
                                    //vels[indx] = ReadV3AdjUnity(vals);
                                    string[] vals = val.data[indx].Split(',');
                                    vels[indx] = ReadV3Adj(vals);

                                    //vels[(x * flow.gridDim2[2] * flow.gridDim2[1]) + ((flow.gridDim2[2] - z - 1) * flow.gridDim2[1]) + y] = ReadV3Adj(vals);
                                }
							}
						}

						flow.framenumber = 0;
						flow.vel.AddRange(vels);
						break;
					}

				default: Debug.Log("Unknown Vel attribute " + val.name); break;
			}
		}
	}

    static void ParseVelUnity(ImageDataSet flow, MegaFlowXMLNode node)
    {
        // we have to rotate the data from a Z up to a Y up
        //Vector3 angles = new Vector3(90.0f, 0.0f, 0.0f);

        for (int i = 0; i < node.values.Count; i++)
        {
            MegaFlowXMLValue val = node.values[i];

            switch (val.name)
            {
                case "data":
                    {
                        //string[] vals = val.value.Split(',');
                        index = 0;

                        flow.vel.Clear();

                        Vector3[] vels = new Vector3[flow.gridDim2[0] * flow.gridDim2[1] * flow.gridDim2[2]];

                        for (int z = 0; z < flow.gridDim2[2]; z++)
                        {
                            for (int y = 0; y < flow.gridDim2[1]; y++)
                            {
                                for (int x = 0; x < flow.gridDim2[0]; x++)
                                {
                                    //int indx = x + (y * flow.gridDim2[0]) + (z * flow.gridDim2[1] * flow.gridDim2[2]);
                                    // the next line must be right for how the data comes from vtk
                                    int indx = x + (y * flow.gridDim2[0]) + (z * flow.gridDim2[0] * flow.gridDim2[1]);
                                    string[] vals = val.data[indx].Split(',');
                                    vels[indx] = ReadV3AdjUnity(vals);
                                    float magnitude = vels[indx].magnitude;
                                    if (magnitude < flow.velMagMin && magnitude != 0.0f)
                                        flow.velMagMin = magnitude;
                                    if (magnitude > flow.velMagMax && magnitude != 0.0f)
                                        flow.velMagMax = magnitude;
                                }
                            }
                        }

                        flow.framenumber = 0;
                        flow.vel.AddRange(vels);
                        break;
                    }

                default: Debug.Log("Unknown Vel attribute " + val.name); break;
            }
        }
    }
}
