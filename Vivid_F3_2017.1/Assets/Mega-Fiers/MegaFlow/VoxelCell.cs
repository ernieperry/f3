﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IVI_CFD
{
    public class VoxelCell
    {
        bool empty;
        Vector3[] cellPoints = new Vector3[8];
        int[] cellPointIds = new int[8];
        int[] indices = new int[3];
        Vector3 center;
        float diagonal;

        public VoxelCell()
        {
            empty = true;
            indices[0] = indices[1] = indices[2] = -1;
            center = Vector3.zero;
            diagonal = 0.0f;
        }

        public void SetCellToEmpty()
        {
            empty = true;
        }

        public void SetCellToFull()
        {
            empty = false;
        }

        public void SetCellIndices(int[] ijk)
        {
            indices[0] = ijk[0];
            indices[1] = ijk[1];
            indices[2] = ijk[2];
        }

        public void GetCellIndices(ref int[] ijk)
        {
            ijk[0] = indices[0];
            ijk[1] = indices[1];
            ijk[2] = indices[2];
        }

        public void SetPoints(ref Vector3[] points)
        {
            cellPoints = points;
        }

        public void SetPointIds(ref int[] pointIds)
        {
            cellPointIds = pointIds;
        }

        public int GetPointId(int index)
        {
            return cellPointIds[index];
        }

        public void SetPointData(int index, int id, Vector3 point)
        {
            cellPointIds[index] = id;
            cellPoints[index] = point;
        }

        public int GetNumberOfPoints()
        {
            return 8;
        }

        void CalculateCenter()
        {
            Vector3 point = Vector3.zero;

            for (int i = 0; i < 8; i++)
            {
                point += cellPoints[i];
            }
            center = point / 8.0f;
        }

        public float CalculateDiagonal()
        {
            Vector3 pt1 = cellPoints[0];
            Vector3 pt2 = cellPoints[7];
            diagonal = Mathf.Sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) +
                                  (pt1.y - pt2.y) * (pt1.y - pt2.y) +
                                  (pt1.z - pt2.z) * (pt1.z - pt2.z));
            return diagonal;
        }
    }
}
