﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IVI_CFD;

//[AddComponentMenu("MegaFlow/Ribbon Control")]
public class MegaFlowStreamlines : MonoBehaviour
{
    public MegaFlow source;
    public List<Ribbon> ribbons = new List<Ribbon>();
    public List<Vector3> seeds = new List<Vector3>();

    ImageDataSet frame;
    public int framenum;
    bool streamlinesDrawn = false;

    public void SetFrame(int f)
    {
        if (source)
        {
            if (f >= 0 && f < source.frames.Count)
            {
                frame = source.frames[f];
                framenum = f;
            }
        }
    }
    
    // Use this for initialization
    void Start ()
    {
        int nbrSeeds = seeds.Count;

        //Vector3 vec = new Vector3(-0.92f, 0.246f, -0.8f);
        //Vector3 vec = new Vector3(-0.92f, 0.346f, -0.8f);
        Vector3 vec = new Vector3(-0.92f, 0.2f, -0.75f);
        seeds.Add(vec);
/*       
        for (int i=0; i<19; i++)
        {
            Vector3 vec1 = vec;
            //vec1.z += (i * 0.1f);
            vec1.z += (i * 0.089f);
            seeds.Add(vec1);
        }
*/      
        CreateStreamlines(source);
        DisplayRibbonsContinuous();
/*
        DrawFlowPoint(source.flow, 72404, 2);
        DrawFlowPoint(source.flow, 72405, 2);
        DrawFlowPoint(source.flow, 72504, 2);
        DrawFlowPoint(source.flow, 72505, 2);

        DrawFlowPoint(source.flow, 62404, 8);
        DrawFlowPoint(source.flow, 62405, 8);
        DrawFlowPoint(source.flow, 62504, 8);
        DrawFlowPoint(source.flow, 62505, 8);
*/
    }
    
    // Update is called once per frame
    void Update()
    {
        
        /*
        if (!streamlinesDrawn)
        {
            RibbonPhysics(source, 0.0f);
            DisplayRibbons(source);
            streamlinesDrawn = true;
        }
        */
    }

    void DisplayRibbonsContinuous()
    {
        for (int i = 0; i < ribbons.Count; i++)
        {
            Ribbon ribbon = ribbons[i];
            ribbon.pointArray = ribbon.points.ToArray();
            int pointCount = ribbon.points.Count;
                     
            GameObject streamline = new GameObject();
            LineRenderer line = streamline.AddComponent<LineRenderer>();
            line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            line.receiveShadows = false;
            line.material = new Material(Shader.Find("Particles/Additive"));
            //line.material = new Material(Shader.Find("Vertex Colors"));
            line.startColor = Color.green;
            line.endColor = Color.green;
            line.startWidth = 0.005f;
            line.endWidth = 0.005f;
            line.numCornerVertices = 5;

            Vector3[] positions = new Vector3[pointCount];
            //GradientColorKey[] colorGrad = new GradientColorKey[numPts];
            for (int k = 0; k < pointCount; k++)
            {
                positions[k] = ribbon.points[k];
                //colorGrad[k].color = ribbon.cols[knt];
                //colorGrad[k].time = 0;
            }
            line.positionCount = pointCount;
            line.SetPositions(positions);

            //Gradient gradient = new Gradient();
            //gradient.SetKeys(colorGrad, alphaGrad);
            //line.colorGradient = gradient;            
        }
    }

    void DisplayRibbons()
    {
        for (int i = 0; i < ribbons.Count; i++)
        {
            Ribbon ribbon = ribbons[i];
            ribbon.pointArray = ribbon.points.ToArray();
            int pointCount = ribbon.points.Count;
            int numEightPts = pointCount / 8;
            int remainderPts = pointCount % 8;

            var startAlpha = new GradientAlphaKey(1, 0);
            var endAlpha = new GradientAlphaKey(1, 1);
            GradientAlphaKey[] alphaGrad = new GradientAlphaKey[2];
            alphaGrad[0] = startAlpha;
            alphaGrad[1] = endAlpha;

            // The maximum number of color keys is 8 a line can have no more than 8 points
            // So each streamline is made up of multiple lines
            for (int j = 0; j < numEightPts+1; j++)
            {
                int index = j * 7;

                int numPts = 8;
                if (j == numEightPts)
                {
                    numPts = remainderPts;
                }

                GameObject streamline = new GameObject();
                LineRenderer line = streamline.AddComponent<LineRenderer>();
                line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                line.receiveShadows = false;
                line.material = new Material(Shader.Find("Particles/Additive"));
                //line.material = new Material(Shader.Find("Vertex Colors"));
                line.startColor = Color.green;
                line.endColor = Color.green;
                line.startWidth = 0.01f;
                line.endWidth = 0.01f;

                Vector3[] positions = new Vector3[numPts];
                //GradientColorKey[] colorGrad = new GradientColorKey[numPts];
                for (int k = 0; k < numPts; k++)
                {
                    int knt = index + k;
                    positions[k] = ribbon.points[knt];
                    //colorGrad[k].color = ribbon.cols[knt];
                    //colorGrad[k].time = 0;
                }
                line.positionCount = numPts;
                line.SetPositions(positions);

                //Gradient gradient = new Gradient();
                //gradient.SetKeys(colorGrad, alphaGrad);
                //line.colorGradient = gradient;
            }
        }
    }
    /*
    static void HexInterpolationFunctions(Vector3 pcoords, float[] sf)
    {
        float r = pcoords[0];
        float s = pcoords[1];
        float t = pcoords[2];
        float rm = 1.0f - r;
        float sm = 1.0f - s;
        float tm = 1.0f - t;
        sf[0] = rm * sm * tm;
        sf[1] = r * sm * tm;
        sf[2] = rm * s * tm;
        sf[3] = r * s * tm;
        sf[4] = rm * sm * t;
        sf[5] = r * sm * t;
        sf[6] = rm * s * t;
        sf[7] = r * s * t;
    }
    */

    //----------------------------------------------------------------------------
    // Compute iso-parametric interpolation functions
    //
    static void HexaInterpolationFunctions(Vector3 pcoords, float[] sf)
    {
        float rm, sm, tm;

        rm = 1.0f - pcoords[0];
        sm = 1.0f - pcoords[1];
        tm = 1.0f - pcoords[2];

        sf[0] = rm * sm * tm;
        sf[1] = pcoords[0] * sm * tm;
        sf[2] = pcoords[0] * pcoords[1] * tm;
        sf[3] = rm * pcoords[1] * tm;
        sf[4] = rm * sm * pcoords[2];
        sf[5] = pcoords[0] * sm * pcoords[2];
        sf[6] = pcoords[0] * pcoords[1] * pcoords[2];
        sf[7] = rm * pcoords[1] * pcoords[2];
    }

    //----------------------------------------------------------------------------
    // Compute iso-parametric interpolation functions
    //
    static void QuadInterpolationFunctions(Vector3 pcoords, float[] sf)
    {
        float rm, sm;

        rm = 1.0f - pcoords[0];
        sm = 1.0f - pcoords[1];

        sf[0] = rm * sm;
        sf[1] = pcoords[0] * sm;
        sf[2] = pcoords[0] * pcoords[1];
        sf[3] = rm * pcoords[1];
    }

    //----------------------------------------------------------------------------
    //
    // Compute iso-parametric interpolation functions
    //
    static void TetraInterpolationFunctions(Vector3 pcoords, float[] sf)
    {
        sf[0] = 1.0f - pcoords[0] - pcoords[1] - pcoords[2];
        sf[1] = pcoords[0];
        sf[2] = pcoords[1];
        sf[3] = pcoords[2];
    }

    //----------------------------------------------------------------------------
    // Compute iso-parametric interpolation functions
    //
    static void WedgeInterpolationFunctions(Vector3 pcoords, float[] sf)
    {
        sf[0] = (1.0f - pcoords[0] - pcoords[1]) * (1.0f - pcoords[2]);
        sf[1] = pcoords[0] * (1.0f - pcoords[2]);
        sf[2] = pcoords[1] * (1.0f - pcoords[2]);
        sf[3] = (1.0f - pcoords[0] - pcoords[1]) * pcoords[2];
        sf[4] = pcoords[0] * pcoords[2];
        sf[5] = pcoords[1] * pcoords[2];
    }

    //----------------------------------------------------------------------------
    // Compute iso-parametric interpolation functions for pyramid
    //
    static void PyramidInterpolationFunctions(Vector3 pcoords, float[] sf)
    {
        float rm, sm, tm;

        rm = 1.0f - pcoords[0];
        sm = 1.0f - pcoords[1];
        tm = 1.0f - pcoords[2];

        sf[0] = rm * sm * tm;
        sf[1] = pcoords[0] * sm * tm;
        sf[2] = pcoords[0] * pcoords[1] * tm;
        sf[3] = rm * pcoords[1] * tm;
        sf[4] = pcoords[2];
    }



    //----------------------------------------------------------------------------
    // This is vtk code from vtkImageData.cxx
    // Convenience function computes the structured coordinates for a point x[3].
    // The voxel is specified by the array ijk[3], and the parametric coordinates
    // in the cell are specified with pcoords[3]. The function returns a 0 if the
    // point x is outside of the volume, and a 1 if inside the volume.
    static bool ComputeStructuredCoordinates(ImageDataSet frame, Vector3 xyz, int[] ijk, ref Vector3 pcoords)
    { 
        // tolerance is needed for 2D data (this is squared tolerance)
        const float tol2 = 1e-8f;
        //
        //  Compute the ijk location
        //
        int isInBounds = 1;
        Vector3 boundMin = frame.origin;
        Vector3 boundMax = frame.origin + frame.size;

        for (int i = 0; i< 3; i++)
        {
            float d = xyz[i] - frame.origin[i];
            float doubleLoc = d / frame.spacing[i];
            // Floor for negative indexes.
            ijk[i] = Mathf.FloorToInt(doubleLoc);
            pcoords[i] = doubleLoc - (1.0f*(ijk[i]));

            int tmpInBounds = 0;
            //int minExt = extent[i * 2];
            //int maxExt = extent[i * 2 + 1];
            int minExt = 0;
            int maxExt = frame.gridDim2[i]-1;

            // check if data is one pixel thick
            if ( minExt == maxExt )
            {
                double dist = xyz[i] - boundMin[i];
                if (dist* dist <= frame.spacing[i]*frame.spacing[i]*tol2)
                {
                    pcoords[i] = 0.0f;
                    ijk[i] = minExt;
                    tmpInBounds = 1;
                }
            }

            // low boundary check
            else if ( ijk[i] < minExt)
            {
                if ( (frame.spacing[i] >= 0 && xyz[i] >= boundMin[i]) ||
                    (frame.spacing[i] < 0 && xyz[i] <= boundMax[i]) )
                {
                    pcoords[i] = 0.0f;
                    ijk[i] = minExt;
                    tmpInBounds = 1;
                }
            }

            // high boundary check
            else if ( ijk[i] >= maxExt )
            {
                if ( (frame.spacing[i] >= 0 && xyz[i] <= boundMax[i]) ||
                    (frame.spacing[i] < 0 && xyz[i] >= boundMin[i]) )
                {
                // make sure index is within the allowed cell index range
                    pcoords[i] = 1.0f;
                    ijk[i] = maxExt - 1;
                    tmpInBounds = 1;
                }
            }

            // else index is definitely within bounds
            else
            {
                tmpInBounds = 1;
            }

            // clear isInBounds if out of bounds for this dimension
            isInBounds = (isInBounds & tmpInBounds);
        }
        
        if (isInBounds == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    static void GetVoxelIndices(int[] ijk, int[] dims, int[] ids)
    {
        ids[0] = (ijk[1] * dims[2] * dims[1]) + (ijk[2] * dims[0]) + ijk[0];
        ids[1] = ids[0] + 1; // i+1, j, k
        ids[2] = ids[0] + dims[0]; // i, j+1, k
        ids[3] = ids[2] + 1; // i+1, j+1, k
        ids[4] = ids[0] + (dims[2] * dims[1]); // i, j, k+1
        ids[5] = ids[4] + 1; // i+1, j, k+1
        ids[6] = ids[4] + dims[0]; // i, j+1, k+1
        ids[7] = ids[6] + 1; // i+1, j+1, k+1
    }

    static bool VectorAt(ImageDataSet frame, Vector3 xyz, ref Vector3 velocity)
    {
        int[] ijk = new int[3];
        Vector3 pcoords = Vector3.zero;
        //Vector3 pcoords;

        if (!ComputeStructuredCoordinates(frame, xyz, ijk, ref pcoords))
        {
            return false;
        }

        int[] dimensions = new int[3];
        dimensions[0] = frame.gridDim2[0];
        dimensions[1] = frame.gridDim2[1];
        dimensions[2] = frame.gridDim2[2];

        int[] voxelIndices = new int[8];
        GetVoxelIndices(ijk, dimensions, voxelIndices);

        // Check voxel points
        bool[] validIndices = new bool[8];
        int cnt = 0;
        for (int i = 0; i < 8; i++)
        {
            validIndices[i] = CheckPointValidity(frame.vel[voxelIndices[i]]);
            if (validIndices[i])
            {
                cnt++;
            }
        }

        velocity = Vector3.zero;
        float[] weights = new float[8];

        if (cnt == 8) // standard hex voxel
        {         
            HexaInterpolationFunctions(pcoords, weights);
        }
        else if (cnt == 7)
        {
            int dummy = 0;
        }
        else if (cnt == 6) // voxel is a wedge
        {
            WedgeInterpolationFunctions(pcoords, weights);
        }
        else if (cnt == 5) // voxel is a pyramid
        {
            PyramidInterpolationFunctions(pcoords, weights);
        }
        else if (cnt == 4) // voxel is a quad
        {
            QuadInterpolationFunctions(pcoords, weights);         
        }

        int idx = 0;
        for (int i = 0; i < 8; i++)
        {
            if (validIndices[i])
            {
                Vector3 voxelvel = frame.vel[voxelIndices[i]];
                velocity += weights[idx++] * voxelvel;
            }
        }

        return true;
    }

    static bool ComputeNextStep(ImageDataSet frame, float delT, ref Vector3 xyz, ref Vector3 velAt)
    {
        //This does Runge-Kutta 2

        // Start with evaluating velocity at the initial point
        //Vector3 velAt = Vector3.zero;

        if (!VectorAt(frame, xyz, ref velAt))
        {
            return false;
        }
        
        //DrawFlowPoint2(xyz, 1);

        // Now find the mid-point
        //Vector3 xtmp = xyz + ((delT / 2.0f) * velAt * 0.5f);  // Bob's change
        Vector3 xtmp = xyz + ((delT / 2.0f) * velAt);
        //DrawFlowPoint2(xtmp, 3);
        // Use the velocity at that point to project
        if (!VectorAt(frame, xtmp, ref velAt))
        {
            return false;
        }

        xyz += delT * velAt;
        //DrawFlowPoint2(xyz, 4);
        if (!VectorAt(frame, xyz, ref velAt))
        {
            return false;
        }

        return true;
    }

    static void StreamIntegrate(MegaFlow mod, Vector3 seed, Ribbon ribbon, int maxSteps, float delT)
    {
        Vector3 xyz = seed;
        Vector3 velAt = Vector3.zero;
        ribbon.points.Add(xyz);

        for (int step=0; step<maxSteps; step++)
        {
            if (!ComputeNextStep(mod.flow, delT, ref xyz, ref velAt))
            {
                break;
            }
            ribbon.points.Add(xyz);
            Color col = mod.GetCol(velAt.magnitude * mod.ribbonscale);
            col.a = mod.ribbonAlpha;
            ribbon.cols.Add(col);
        }
    }

    void CreateStreamlines(MegaFlow mod)
    {
        int nbrSeeds = seeds.Count;
        //float delT = mod.Dt;
        //float delT = 0.004f; // used with file F3-111.flw
        //float delT = 0.002f;  // used with file F3-211.flw
        float delT = 0.008f;  // used with file F3-211.flw
        int maxSteps = 1000;

        for (int i=0; i<nbrSeeds; i++)
        {
            Ribbon ribbon = new Ribbon();

            StreamIntegrate(mod, seeds[i], ribbon, maxSteps, delT);
            ribbons.Add(ribbon);
        }
    }

    static bool CheckPointValidity(Vector3 vel)
    {
        float tol = 0.000001f;
        float value = Mathf.Abs(vel.x) + Mathf.Abs(vel.y) + Mathf.Abs(vel.z);

        if (value < tol)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void DrawFlowPoint(MegaFlowFrame frame, int number, int color)
    {
        int[] dimensions = new int[3];
        dimensions[0] = frame.gridDim2[0];
        dimensions[1] = frame.gridDim2[1];
        dimensions[2] = frame.gridDim2[2];
         
        int Z = number / (dimensions[0] * dimensions[1]);
        int XYnumber = number - ((dimensions[0] * dimensions[1]) * Z);
        int Y = XYnumber / dimensions[0];
        int X = XYnumber - (dimensions[0] * Y);

        int indx = X + (Y * dimensions[0]) + (Z * dimensions[1] * dimensions[2]);

        // get the coordinates for the point
        Vector3 p = Vector3.zero;
        p.z = frame.origin.z + (Z * frame.spacing.z);
        p.y = frame.origin.y + (Y * frame.spacing.y);
        p.x = frame.origin.x + (X * frame.spacing.x);
        
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = p;
        if (color == 1)
        {
            sphere.GetComponent<Renderer>().material.color = Color.blue;
        }
        else if (color == 2)
        {
            sphere.GetComponent<Renderer>().material.color = Color.red;
        }
        else if (color == 3)
        {
            sphere.GetComponent<Renderer>().material.color = Color.green;
        }
        else if (color == 4)
        {
            sphere.GetComponent<Renderer>().material.color = Color.white;
        }
        else if (color == 5)
        {
            sphere.GetComponent<Renderer>().material.color = Color.black;
        }
        else if (color == 6)
        {
            sphere.GetComponent<Renderer>().material.color = Color.yellow;
        }
        else if (color == 7)
        {
            sphere.GetComponent<Renderer>().material.color = Color.cyan;
        }
        else if (color == 8)
        {
            sphere.GetComponent<Renderer>().material.color = Color.gray;
        }
        sphere.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

        /*
        GameObject sphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere2.transform.position = frame.origin;
        sphere2.GetComponent<Renderer>().material.color = Color.blue;
        sphere2.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        */
    }

    static void DrawFlowPoint2(Vector3 xyz, int color)
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = xyz;
        if (color == 1)
        {
            sphere.GetComponent<Renderer>().material.color = Color.blue;
        }
        else if (color == 2)
        {
            sphere.GetComponent<Renderer>().material.color = Color.red;
        }
        else if (color == 3)
        {
            sphere.GetComponent<Renderer>().material.color = Color.green;
        }
        else if (color == 4)
        {
            sphere.GetComponent<Renderer>().material.color = Color.white;
        }
        else if (color == 5)
        {
            sphere.GetComponent<Renderer>().material.color = Color.black;
        }
        else if (color == 6)
        {
            sphere.GetComponent<Renderer>().material.color = Color.yellow;
        }
        else if (color == 7)
        {
            sphere.GetComponent<Renderer>().material.color = Color.cyan;
        }
        else if (color == 8)
        {
            sphere.GetComponent<Renderer>().material.color = Color.gray;
        }
        sphere.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

        /*
        GameObject sphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere2.transform.position = frame.origin;
        sphere2.GetComponent<Renderer>().material.color = Color.blue;
        sphere2.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        */
    }
}

