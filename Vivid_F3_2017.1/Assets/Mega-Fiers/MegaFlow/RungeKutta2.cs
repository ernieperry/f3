﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace IVI_CFD
{
   /**
  * Given initial values, xprev , initial time, t and a requested time
  * interval, delT calculate values of x at t+delT (xnext).
  * delTActual is always equal to delT.
  * Since this class can not provide an estimate for the error error
  * is set to 0.
  * maxStep, minStep and maxError are unused.
  * This method returns an error code representing the nature of
  * the failure:
  * OutOfDomain = 1,
  * NotInitialized = 2,
  * UnexpectedValue = 3
  */
    public class RungeKutta2 : InitialProblemSolver
    {
        public int ComputeNextStep(Vector3 xprev, Vector3 dxprev, ref Vector3 xnext, float t,
                                   float delT, ref float delTActual, ref float error)                        
        {
            delTActual = 0.0f;
            error = 0.0f;

            if (!this.dataset)
            {
                return (int)ErrorCodes.NOT_INITIALIZED;
            }

            if (!this.Initialized)
            {
                return (int)ErrorCodes.NOT_INITIALIZED;
            }

            StepPoints prevPt;
            prevPt.point = xprev;
            prevPt.time = t;
            this.Vals.Add(prevPt);


            // Obtain the derivatives dx at x
            if (dxprev != null)
            {
                this.Derivs.Add(dxprev);
            }

            Vector3 velocity;
            if (!this.interpolator.GetFieldValuesAt(this.Vals.Last().point, out velocity))
            {
                //memcpy(xnext, this->Vals, (numVals - 1) * sizeof(double));
                xnext = this.Vals.Last().point;
                return (int)ErrorCodes.OUT_OF_DOMAIN;
            }
            else
                this.Derivs.Add(velocity);

            // Half-step
            StepPoints newPt;
            newPt.point = xprev + delT / 2.0f * this.Derivs.Last() ;
            newPt.time = t + delT / 2.0f;
            this.Vals.Add(newPt);

            // Obtain the derivatives at x + dt/2 * dx
            if (!this.interpolator.GetFieldValuesAt(this.Vals.Last().point, out velocity))
            {
                //memcpy(xnext, this->Vals, (numVals - 1) * sizeof(double));
                xnext = this.Vals.Last().point;
                delTActual = delT / 2.0f; // we've only taken half of a time step
                return (int)ErrorCodes.OUT_OF_DOMAIN;
            }
            else
                this.Derivs.Add(velocity);

            // Calculate x using improved values of derivatives
            xnext = xprev + delT * this.Derivs.Last();
            delTActual = delT;

            return 0;
        }
    }
}
