﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.FastLineRenderer;

namespace IVI_CFD
{
    public class StreamlineController : MonoBehaviour
    {
        public MegaFlow source;
        private List<GameObject> streamlines = new List<GameObject>(6);
        public List<Ribbon> ribbons = new List<Ribbon>();
        public List<Vector3> seeds = new List<Vector3>();
        private GameObject parentObj;

        ImageDataSet frame;
        public int framenum;
        //bool streamlinesDrawn = false;

        [SerializeField] FastLineRenderer LineRenderer;

        public void SetFrame(int f)
        {
            if (source)
            {
                if (f >= 0 && f < source.frames.Count)
                {
                    frame = source.frames[f];
                    framenum = f;
                }
            }
        }

        // Use this for initialization
        void Start()
        {
            parentObj = GameObject.FindGameObjectWithTag("streamline_parent");

            int nbrSeeds = seeds.Count;
            Vector3 vec;

            vec = new Vector3(-0.92f, 0.246f, -0.75f);
            seeds.Add(vec);
            vec = new Vector3(-0.92f, 0.346f, -0.75f);
            seeds.Add(vec);
            vec = new Vector3(-0.92f, 0.2f, -0.75f);
            seeds.Add(vec);
            vec = new Vector3(-0.92f, 0.2f, -0.5f);
            seeds.Add(vec);
            /*       
                    for (int i=0; i<19; i++)
                    {
                        Vector3 vec1 = vec;
                        //vec1.z += (i * 0.1f);
                        vec1.z += (i * 0.089f);
                        seeds.Add(vec1);
                    }
            */
            CreateStreamlines(source);
            DisplayRibbonsContinuous();
            /*
                    DrawFlowPoint(source.flow, 72404, 2);
                    DrawFlowPoint(source.flow, 72405, 2);
                    DrawFlowPoint(source.flow, 72504, 2);
                    DrawFlowPoint(source.flow, 72505, 2);

                    DrawFlowPoint(source.flow, 62404, 8);
                    DrawFlowPoint(source.flow, 62405, 8);
                    DrawFlowPoint(source.flow, 62504, 8);
                    DrawFlowPoint(source.flow, 62505, 8);
            */
        }

        // Update is called once per frame
        void Update()
        {

        }

        void CreateStreamlines(MegaFlow mod)
        {
            ImageDataSet dataset = mod.flow;
            StreamTracer tracer = new StreamTracer();
            tracer.SetMaximumPropagation(4.5f);
            tracer.SetIntegrationDirection(0);

            tracer.RequestData(dataset, seeds, ribbons);
            CalculateColors(dataset, ribbons);
        }

        void DisplayRibbonsContinuous()
        {
            //System.Type changeheight2 = System.Type.GetType("ChangeHeight2");
            //System.Type changerotation2 = System.Type.GetType("ChangeRotation2");

            for (int i = 0; i < ribbons.Count; i++)
            {
                Ribbon ribbon = ribbons[i];
                //ribbon.pointArray = ribbon.points.ToArray();
                int pointCount = ribbon.points.Count;

                GameObject streamline = new GameObject();
                // Add the streamlines as child. The script to transform is attached to the parent
                if (parentObj != null)
                    streamline.transform.parent = parentObj.transform;
                streamline.name = "streamline";
                // trying to add the script to the streamline
                //streamline.AddComponent(changeheight2);
                //streamline.AddComponent(changerotation2);
                streamlines.Add(streamline);

                
                FastLineRendererProperties props = new FastLineRendererProperties();
                FastLineRenderer r = FastLineRenderer.CreateWithParent(streamline, LineRenderer);
                //FastLineRenderer r = streamline.AddComponent<FastLineRenderer>();

                r.Material.EnableKeyword("DISABLE_CAPS");
                r.SetCapacity(8192 * FastLineRenderer.VerticesPerLine);
                //r.SetCapacity(16384 * FastLineRenderer.VerticesPerLine);
                r.Turbulence = 0.0f;
                r.BoundsScale = new Vector3(1.0f, 1.0f, 1.0f);
               
                // animation time per segment - set to 0 to have the whole thing appear at once
                //const float animationTime = 0.0f;

                // create properties - do this once, before your loop
                props.Radius = 0.005f;

                // add the list of line points
                int count = 0;
                r.AddLine(props, ribbon.points, (FastLineRendererProperties _props) =>
                {
                    props.Color = ribbon.cols[count++];

                    // animate this line segment in later
                    //props.AddCreationTimeSeconds(animationTime);

                }, true, true);
                r.Apply();

                /*
                LineRenderer line = streamline.AddComponent<LineRenderer>();
                line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                line.receiveShadows = false;
                line.material = new Material(Shader.Find("Particles/Additive"));
                //line.material = new Material(Shader.Find("Vertex Colors"));
                line.startColor = Color.green;
                line.endColor = Color.green;
                line.startWidth = 0.005f;
                line.endWidth = 0.005f;
                line.numCornerVertices = 5;
                line.useWorldSpace = false;
         
                Vector3[] positions = new Vector3[pointCount];
                GradientColorKey[] colorGrad = new GradientColorKey[pointCount];
                for (int k = 0; k < pointCount; k++)
                {
                    positions[k] = ribbon.points[k];
                    //colorGrad[k].color = ribbon.cols[k];
                    //colorGrad[k].time = 0;
                }
                line.positionCount = pointCount;
                line.SetPositions(positions);

                //Gradient gradient = new Gradient();
                //gradient.SetKeys(colorGrad, alphaGrad);
                //line.colorGradient = gradient;  
               */
            }
        }

        void DisplayRibbons()
        {
            for (int i = 0; i < ribbons.Count; i++)
            {
                Ribbon ribbon = ribbons[i];
                ribbon.pointArray = ribbon.points.ToArray();
                int pointCount = ribbon.points.Count;
                int numEightPts = pointCount / 8;
                int remainderPts = pointCount % 8;

                var startAlpha = new GradientAlphaKey(1, 0);
                var endAlpha = new GradientAlphaKey(1, 1);
                GradientAlphaKey[] alphaGrad = new GradientAlphaKey[2];
                alphaGrad[0] = startAlpha;
                alphaGrad[1] = endAlpha;

                // The maximum number of color keys is 8 a line can have no more than 8 points
                // So each streamline is made up of multiple lines
                for (int j = 0; j < numEightPts + 1; j++)
                {
                    int index = j * 7;

                    int numPts = 8;
                    if (j == numEightPts)
                    {
                        numPts = remainderPts;
                    }

                    GameObject streamline = new GameObject();
                    LineRenderer line = streamline.AddComponent<LineRenderer>();
                    line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    line.receiveShadows = false;
                    line.material = new Material(Shader.Find("Particles/Additive"));
                    //line.material = new Material(Shader.Find("Vertex Colors"));
                    line.startColor = Color.green;
                    line.endColor = Color.green;
                    line.startWidth = 0.01f;
                    line.endWidth = 0.01f;

                    Vector3[] positions = new Vector3[numPts];
                    //GradientColorKey[] colorGrad = new GradientColorKey[numPts];
                    for (int k = 0; k < numPts; k++)
                    {
                        int knt = index + k;
                        positions[k] = ribbon.points[knt];
                        //colorGrad[k].color = ribbon.cols[knt];
                        //colorGrad[k].time = 0;
                    }
                    line.positionCount = numPts;
                    line.SetPositions(positions);

                    //Gradient gradient = new Gradient();
                    //gradient.SetKeys(colorGrad, alphaGrad);
                    //line.colorGradient = gradient;
                }
            }
        }
 
        void DrawFlowPoint(MegaFlowFrame frame, int number, int color)
        {
            int[] dimensions = new int[3];
            dimensions[0] = frame.gridDim2[0];
            dimensions[1] = frame.gridDim2[1];
            dimensions[2] = frame.gridDim2[2];

            int Z = number / (dimensions[0] * dimensions[1]);
            int XYnumber = number - ((dimensions[0] * dimensions[1]) * Z);
            int Y = XYnumber / dimensions[0];
            int X = XYnumber - (dimensions[0] * Y);

            int indx = X + (Y * dimensions[0]) + (Z * dimensions[1] * dimensions[2]);

            // get the coordinates for the point
            Vector3 p = Vector3.zero;
            p.z = frame.origin.z + (Z * frame.spacing.z);
            p.y = frame.origin.y + (Y * frame.spacing.y);
            p.x = frame.origin.x + (X * frame.spacing.x);

            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = p;
            if (color == 1)
            {
                sphere.GetComponent<Renderer>().material.color = Color.blue;
            }
            else if (color == 2)
            {
                sphere.GetComponent<Renderer>().material.color = Color.red;
            }
            else if (color == 3)
            {
                sphere.GetComponent<Renderer>().material.color = Color.green;
            }
            else if (color == 4)
            {
                sphere.GetComponent<Renderer>().material.color = Color.white;
            }
            else if (color == 5)
            {
                sphere.GetComponent<Renderer>().material.color = Color.black;
            }
            else if (color == 6)
            {
                sphere.GetComponent<Renderer>().material.color = Color.yellow;
            }
            else if (color == 7)
            {
                sphere.GetComponent<Renderer>().material.color = Color.cyan;
            }
            else if (color == 8)
            {
                sphere.GetComponent<Renderer>().material.color = Color.gray;
            }
            sphere.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

            /*
            GameObject sphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere2.transform.position = frame.origin;
            sphere2.GetComponent<Renderer>().material.color = Color.blue;
            sphere2.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            */
        }

        static void DrawFlowPoint2(Vector3 xyz, int color)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = xyz;
            if (color == 1)
            {
                sphere.GetComponent<Renderer>().material.color = Color.blue;
            }
            else if (color == 2)
            {
                sphere.GetComponent<Renderer>().material.color = Color.red;
            }
            else if (color == 3)
            {
                sphere.GetComponent<Renderer>().material.color = Color.green;
            }
            else if (color == 4)
            {
                sphere.GetComponent<Renderer>().material.color = Color.white;
            }
            else if (color == 5)
            {
                sphere.GetComponent<Renderer>().material.color = Color.black;
            }
            else if (color == 6)
            {
                sphere.GetComponent<Renderer>().material.color = Color.yellow;
            }
            else if (color == 7)
            {
                sphere.GetComponent<Renderer>().material.color = Color.cyan;
            }
            else if (color == 8)
            {
                sphere.GetComponent<Renderer>().material.color = Color.gray;
            }
            sphere.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

            /*
            GameObject sphere2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere2.transform.position = frame.origin;
            sphere2.GetComponent<Renderer>().material.color = Color.blue;
            sphere2.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
            */
        }

        private void CalculateColors(ImageDataSet dataset, List<Ribbon> ribbons)
        {
            float min = dataset.velMagMin;
            //float max = dataset.velMagMax;
            float max = 55.0f;
            //Color minColor = Color.blue;
            //Color maxColor = Color.red;
            Color minColor = Color.white;
            Color maxColor = Color.blue;

            float range = max - min;

            for (int i=0; i<ribbons.Count; i++)
            {
                for (int j=0; j<ribbons[i].colorScalar.Count; j++)
                {
                    float scalar = ribbons[i].colorScalar[j];
                    float percent = (scalar - min) / range;
                    Color pointColor = InterpolateColor(minColor, maxColor, percent);
                    ribbons[i].cols.Add(pointColor);
                }
            }

        }

        private Color InterpolateColor(Color c1, Color c2, float p)
        {
            //This function uses a Unity function called color lerp that needs three arguments;
            //c1 and c2; colors to interpolate between
            //p The percentage of interpolation
            return Color.Lerp(c1, c2, p);
        }
    }
}

