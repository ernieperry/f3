﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IVI_CFD
{
    public class StreamTracer
    {
        ImageDataSet dataset = null;
        VelocityFieldInterpolator interpolator = null;

        static float EPSILON = 1.0E-8f;

        public enum Direction { FORWARD, BACKWARD, BOTH }
        public enum Units { LENGTH_UNIT, CELL_LENGTH_UNIT}
        public enum Solvers
        {
            RUNGE_KUTTA2,
            RUNGE_KUTTA4,
            RUNGE_KUTTA45,
            NONE,
            UNKNOWN
        }

        public enum ReasonForTermination
        {
            OUT_OF_DOMAIN,
            NOT_INITIALIZED,
            UNEXPECTED_VALUE,
            OUT_OF_LENGTH,
            OUT_OF_STEPS,
            STAGNATION
        }

        public struct IntervalInformation
        {
            public float Interval;
            public Units Unit;
        }

        Direction IntegrationDirection = Direction.FORWARD;
        Solvers IntegrationType = Solvers.RUNGE_KUTTA2;
        float MaximumPropagation = 1.0f;
        Units IntegrationStepUnit = Units.CELL_LENGTH_UNIT;

        /* These are the inital values found in the vtk code
        float InitialIntegrationStep = 0.5f;
        float MinimumIntegrationStep = 1.0E-2f;
        float MaximumIntegrationStep = 1.0f;
        */
        // These are the initial values set by Paraview
        float InitialIntegrationStep = 0.2f;
        float MinimumIntegrationStep = 0.01f;
        float MaximumIntegrationStep = 0.5f;
        float MaximumError = 1.0E-6f;
        int MaximumNumberOfSteps = 20000;
        float TerminalSpeed = EPSILON;

        bool ComputeVorticity = false;
        float RotationScale = 1.0f;

        float LastUsedStepSize = 0.0f;
        bool GenerateNormalsInIntegrate = true;
        int InterpolatorPrototype = 0;

        bool HasMatchingPointAttributes = true;
        bool SurfaceStreamlines = false;

        List<ReasonForTermination> terminationValues = new List<ReasonForTermination>();

        public void SetMaximumPropagation(float value)
        {
            MaximumPropagation = value;
        }

        public void SetIntegratorType(int type)
        {
            IntegrationType = (Solvers)type;
        }

        public Solvers GetIntegratorType()
        {
            return IntegrationType;
        }

        public void SetIntegrationDirection(int type)
        {
            IntegrationDirection = (Direction)type;
        }

        public Direction GetIntegrationDirection()
        {
            return IntegrationDirection;
        }

        void SetIntegrationStepUnit(Units unit)
        {
            if (unit != Units.LENGTH_UNIT && unit != Units.CELL_LENGTH_UNIT)
            {
                unit = Units.CELL_LENGTH_UNIT;
            }

            if (unit == this.IntegrationStepUnit)
            {
                return;
            }

            this.IntegrationStepUnit = unit;
            //this->Modified(); I don't know if I need this yet
        }

        float ConvertToLength( float interval, Units unit, float cellLength)
        {
            float retVal = 0.0f;
            if (unit == Units.LENGTH_UNIT)
            {
                retVal = interval;
            }
            else
            if (unit == Units.CELL_LENGTH_UNIT)
            {
                retVal = interval * cellLength;
            }
            return retVal;
        }

        void ConvertIntervals(ref float step, ref float minStep,
                              ref float maxStep, int direction, float cellLength)
        {
            minStep = maxStep = step =
              direction * this.ConvertToLength(this.InitialIntegrationStep, this.IntegrationStepUnit, cellLength);

            if (this.MinimumIntegrationStep > 0.0f)
            {
                minStep = this.ConvertToLength(this.MinimumIntegrationStep, this.IntegrationStepUnit, cellLength);
            }

            if (this.MaximumIntegrationStep > 0.0f)
            {
                maxStep = this.ConvertToLength(this.MaximumIntegrationStep, this.IntegrationStepUnit, cellLength);
            }
        }

        public bool RequestData(ImageDataSet frame, List<Vector3> seeds, List<Ribbon> ribbons)
        {
            dataset = frame;

            Vector3 lastPoint = Vector3.zero;

            interpolator = new VelocityFieldInterpolator(dataset);
            this.Integrate(seeds, lastPoint, ribbons);

            for (int i=0; i<terminationValues.Count; i++)
            {
                ReasonForTermination reason = terminationValues[i];
                int dummy = 0;
            }

            return false;
        }

        void Integrate(List<Vector3> seeds, Vector3 lastPoint, List<Ribbon> ribbons)
        {
            int nbrLines = seeds.Count;
            float propagation = 0.0f;
            int numSteps = 0;          
            float integrationTime = 0.0f;

            int numPtsTotal = 0;
            int direction = 1;
            List<float> time = new List<float>();
            float[] weights = new float[8];
            Vector3 velocity;

            //float delT = 0.0f;

            // As I add integrators, I will have to send in which one I want
            RungeKutta2 integrator = new RungeKutta2();
            if (integrator == null)
                return;

            integrator.SetFunctionSet(dataset);
            integrator.SetInterpolator(interpolator);

            bool shouldAbort = false;

            // Loop through each streamline to be created
            for (int j = 0; j < nbrLines; j++)
            {
                List<Vector3> outputPoints = new List<Vector3>();
                List<Vector3> outputVelocity = new List<Vector3>();
                List<float> outputVelocityMagnitude = new List<float>();

                switch (this.IntegrationDirection)
                {
                    case Direction.FORWARD:
                        direction = 1;
                        break;
                    case Direction.BACKWARD:
                        direction = -1;
                        break;
                        //case Direction.BOTH:
                        //    direction = 3;
                        //    break;
                }

                // temporary variables used in the integration
                Vector3 point1, point2;
                int numPoints = 0;

                // Set the initial seed points
                point1 = seeds[j];
                point2 = point1;

                // Clear the last cell to avoid starting a search from the last point in the streamline
                interpolator.ClearLastCellId();

                // Get the velocity at the given first point
                interpolator.GetFieldValuesAt(point1, out velocity);
                outputVelocity.Add(velocity);

                numPoints++;
                numPtsTotal++;

                Vector3 lastInsertedPoint = point1;
                outputPoints.Add(point1);
                outputVelocityMagnitude.Add(velocity.magnitude);
                time.Add(integrationTime);

                // We will always pass an arc-length step size to the integrator.
                // If the user specifies a step size in cell length unit, we will
                // have to convert it to arc length.
                IntervalInformation stepSize;  // either positive or negative
                stepSize.Unit = Units.LENGTH_UNIT;
                stepSize.Interval = 0.0f;
                IntervalInformation aStep; // always positive
                aStep.Unit = Units.LENGTH_UNIT;
                float minStep = 0, maxStep = 0;
                float stepTaken = 0.0f;
                float speed = 0.0f;
                float cellLength = 0.0f;
                int tmp;
                ReasonForTermination retVal = ReasonForTermination.OUT_OF_LENGTH;


                cellLength = interpolator.cell.CalculateDiagonal();
                speed = velocity.magnitude;
                // Never call conversion methods is speed is zero
                // Convert intervals to arc-length unit
                if (speed != 0.0f)
                {
                    ConvertIntervals(ref stepSize.Interval, ref minStep, ref maxStep, direction, cellLength);
                }

                interpolator.GetLastWeights(ref weights);
                //interpolator.InterpolatePoint(weights);  // Currently not used

                // Currently not used
                if (this.ComputeVorticity)
                {
                    // Need to add this code
                }

                float error = 0.0f;

                // Integrate until the maximum propagation length is reached,
                // maximum number of steps is reached or until a boundary is encountered.
                // Begin Integration
                while (propagation < MaximumPropagation)
                {
                    if (numSteps > MaximumNumberOfSteps)
                    {
                        retVal = ReasonForTermination.OUT_OF_STEPS;
                        break;
                    }

                    /*
                    if (numSteps++ % 1000 == 1)
                    {
                        shouldAbort = true;
                        break;
                    }
                    */

                    // Never call conversion methods if speed == 0
                    if ((speed == 0.0f) || (speed <= TerminalSpeed))
                    {
                        retVal = ReasonForTermination.STAGNATION;
                        break;
                    }

                    // If, with the next step, propagation will be larger than
                    // max, reduce it so that it is (approximately) equal to max.
                    aStep.Interval = Mathf.Abs(stepSize.Interval);

                    if ((propagation + aStep.Interval) > MaximumPropagation)
                    {
                        aStep.Interval = MaximumPropagation - propagation;
                        if (stepSize.Interval >= 0)
                        {
                            stepSize.Interval = ConvertToLength(aStep.Interval, aStep.Unit, cellLength);
                        }
                        else
                        {
                            stepSize.Interval = ConvertToLength(aStep.Interval, aStep.Unit, cellLength) * (-1.0f);
                        }
                        maxStep = stepSize.Interval;
                    }
                    LastUsedStepSize = stepSize.Interval;

                    // Calculate the next step using the integrator provided
                    // Break if the next point is out of bounds.
                    interpolator.SetNormalizeVector(true);
                    Vector3 dummy = Vector3.zero;
                    float t = 0.0f;
                    tmp = integrator.ComputeNextStep(point1, dummy, ref point2, t, stepSize.Interval,
                                                     ref stepTaken, ref error);

                    interpolator.SetNormalizeVector(false);
                    if (tmp != 0)
                    {
                        retVal = (ReasonForTermination)tmp;
                        lastPoint = point2;
                        break;
                    }

                    // This is the next starting point
                    /*
                    if (this->SurfaceStreamlines && surfaceFunc != NULL)
                    {
                        if (surfaceFunc->SnapPointOnCell(point2, point1) != 1)
                        {
                            retVal = OUT_OF_DOMAIN;
                            lastPoint = point2;
                            break;
                        }
                    }
                    else
                    */
                    {
                        point1 = point2;
                    }

                    // Interpolate the velocity at the next point
                    if (!interpolator.GetFieldValuesAt(point2, out velocity))
                    {
                        retVal = ReasonForTermination.OUT_OF_DOMAIN;
                        lastPoint = point2;
                        break;
                    }
                    outputVelocity.Add(velocity);

                    // It is not enough to use the starting point for stagnation calculation
                    // Use average speed to check if it is below stagnation threshold
                    float speed2 = velocity.magnitude;
                    if ((speed + speed2) / 2 <= this.TerminalSpeed)
                    {
                        retVal = ReasonForTermination.STAGNATION;
                        break;
                    }

                    integrationTime += stepTaken / speed;
                    // Calculate propagation (using the same units as MaximumPropagation
                    propagation += Mathf.Abs(stepSize.Interval);

                    // We don't need this now EP
                    // Make sure we use the dataset found by the vtkAbstractInterpolatedVelocityField
                    //input = func->GetLastDataSet();
                    //inputPD = input->GetPointData();
                    //inVectors = input->GetAttributesAsFieldData(vecType)->GetArray(vecName);

                    // Calculate cell length and speed to be used in unit conversions
                    //cellLength = fieldData.cell.CalculateDiagonal(); The cell diagonal length will not change so we won't recalculate it
                    speed = speed2;

                    // Point is valid. Insert it.
                    numPoints++;
                    numPtsTotal++;

                    lastInsertedPoint = point1;
                    outputPoints.Add(point1);
                    outputVelocityMagnitude.Add(velocity.magnitude);
                    time.Add(integrationTime);

                    // We are only interested in the primary attribute (velocity for now)
                    // Interpolate all point attributes on current point
                    //func->GetLastWeights(weights);
                    //InterpolatePoint(outputPD, inputPD, nextPoint, cell->PointIds, weights, this->HasMatchingPointAttributes);

                    // Never call conversion methods if speed == 0
                    if ((speed == 0) || (speed <= this.TerminalSpeed))
                    {
                        retVal = ReasonForTermination.STAGNATION;
                        break;
                    }

                    // Convert all intervals to arc length
                    ConvertIntervals(ref stepSize.Interval, ref minStep, ref maxStep, direction, cellLength);

                    // If the solver is adaptive and the next step size (stepSize.Interval)
                    // that the solver wants to use is smaller than minStep or larger
                    // than maxStep, re-adjust it. This has to be done every step
                    // because minStep and maxStep can change depending on the cell
                    // size (unless it is specified in arc-length unit)
                    /*
                    if (integrator.IsAdaptive())
                    {
                        if (Mathf.Abs(stepSize.Interval) < Mathf.Abs(minStep))
                        {
                            stepSize.Interval = Mathf.Abs(minStep) *
                                                  stepSize.Interval / Mathf.Abs(stepSize.Interval);
                        }
                        else if (Mathf.Abs(stepSize.Interval) > Mathf.Abs(maxStep))
                        {
                            stepSize.Interval = Mathf.Abs(maxStep) *
                                                  stepSize.Interval / Mathf.Abs(stepSize.Interval);
                        }
                    }
                    else
                    {
                        stepSize.Interval = step;
                    }
                    */
                    // End Integration
                }

                /*
                if (shouldAbort)
                {
                    break;
                }
                */

                if (numPoints > 1)
                {
                    Ribbon streamline = new Ribbon();
                    streamline.points = new List<Vector3>(outputPoints);
                    streamline.colorScalar = new List<float>(outputVelocityMagnitude);
                    ribbons.Add(streamline);

                    terminationValues.Add(retVal);
                }

                // Initialize these to 0 before starting the next line.
                // The values passed in the function call are only used
                // for the first line.
                // EP these are passed out in vtk, don't know if we need them
                //inPropagation = propagation;
                //inNumSteps = numSteps;
                //inIntegrationTime = integrationTime;

                // Reset for the next line
                propagation = 0;
                numSteps = 0;
                integrationTime = 0;
                time.Clear();
                outputPoints.Clear();
                outputVelocity.Clear();
                outputVelocityMagnitude.Clear();
            }
            
        }



        static bool CheckPointValidity(Vector3 vel)
        {
            float tol = 0.000001f;
            float value = Mathf.Abs(vel.x) + Mathf.Abs(vel.y) + Mathf.Abs(vel.z);

            if (value < tol)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}

