﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IVI_CFD
{
    public class VelocityFieldInterpolator
    {
        int CacheHit;
        int CacheMiss;
        int WeightsSize;
        bool Caching;
        bool NormalizeVector;
        int VectorsType;
        float[] Weights = new float[8];
        Vector3 LastPCoords;
        int LastSubId;
        public int LastCellId;  // This is the index for the attached cell
        public VoxelCell cell = null;
        VoxelCell GenCell = null;
        public ImageDataSet dataset = null;


        public VelocityFieldInterpolator()
        {
            LastCellId = -1;
        }

        public VelocityFieldInterpolator(ImageDataSet data)
        {
            dataset = data;
            LastCellId = -1;
        }

        public void SetImageData(ImageDataSet data)
        {
            dataset = data;
            LastCellId = -1;
        }

        //----------------------------------------------------------------------------
        // Compute iso-parametric interpolation functions
        //
        void HexaInterpolationFunctions(Vector3 pcoords, float[] sf)
        {
            float rm, sm, tm;

            rm = 1.0f - pcoords[0];
            sm = 1.0f - pcoords[1];
            tm = 1.0f - pcoords[2];

            /*
            sf[0] = rm * sm * tm;
            sf[1] = pcoords[0] * sm * tm;
            sf[2] = pcoords[0] * pcoords[1] * tm;
            sf[3] = rm * pcoords[1] * tm;
            sf[4] = rm * sm * pcoords[2];
            sf[5] = pcoords[0] * sm * pcoords[2];
            sf[6] = pcoords[0] * pcoords[1] * pcoords[2];
            sf[7] = rm * pcoords[1] * pcoords[2];
            */
            sf[0] = rm * sm * tm;
            sf[1] = pcoords[0] * sm * tm;
            sf[5] = pcoords[0] * pcoords[1] * tm;
            sf[4] = rm * pcoords[1] * tm;
            sf[2] = rm * sm * pcoords[2];
            sf[3] = pcoords[0] * sm * pcoords[2];
            sf[7] = pcoords[0] * pcoords[1] * pcoords[2];
            sf[6] = rm * pcoords[1] * pcoords[2];
            
        }

        int ComputeCellId(int[] ijk)
        {
            int indx = ijk[0] + (ijk[2] * (this.dataset.gridDim2[0] - 1)) + (ijk[1] * (this.dataset.gridDim2[0] - 1) * (this.dataset.gridDim2[2] - 1));
            return indx;
        }

        public bool GetFieldValuesAt(Vector3 xyz, out Vector3 velocity)
        {
            int numpts = 0;
            float[] weights = new float[8];
            velocity = Vector3.zero;

            if (!FindAndUpdateCell(xyz, weights))
            {     
                return false;
            }
  
            numpts = this.cell.GetNumberOfPoints();

            for (int i = 0; i < numpts; i++)
            {
                int id = this.cell.GetPointId(i);
                float wght = weights[i];  // for debugging only
                Vector3 voxelvel = this.dataset.vel[id];
                velocity += weights[i] * voxelvel;
            }

            if (this.NormalizeVector == true)
                velocity.Normalize();

            return true;
        }

        bool FindAndUpdateCell(Vector3 xyz, float[] weights)
        {
            const float tol = 1e-8f;
            float tol2;
            //double dist2 = 0.0f;
            int[] ijk = new int[3];
            this.cell = new VoxelCell();

            float diagonal = GetFieldDiagonal();
            tol2 = diagonal * diagonal * tol;

            bool found = false;
            if (this.Caching)
            {
                bool outVal = false;

                // See if the point is in the cached cell
                if (this.LastCellId != -1)
                {
                }
            }

            if (!found)
            {
                if (!FindCell(xyz, tol2, ref LastPCoords, ijk, weights))
                {
                    return false;
                }
                int cellid = ComputeCellId(ijk);
                this.LastCellId = cellid;
                
                if (this.LastCellId != -1 && CheckPCoords(LastPCoords))
                {
                    this.cell.SetCellIndices(ijk);
                    GetCell(this.LastCellId, ref this.cell);
                }           
            }
            return true;
        }

        bool FindCell(Vector3 xyz, float tol, ref Vector3 pcoords, int[] ijk, float[] weights)
        {
            if (!ComputeStructuredCoordinates(xyz, ijk, ref pcoords))
            {
                // fill in the other code from vtk later
                return false;
            }
            HexaInterpolationFunctions(pcoords, weights);

            return true;
        }

        //----------------------------------------------------------------------------
        // This is vtk code from vtkImageData.cxx
        // Convenience function computes the structured coordinates for a point x[3].
        // The voxel is specified by the array ijk[3], and the parametric coordinates
        // in the cell are specified with pcoords[3]. The function returns a 0 if the
        // point x is outside of the volume, and a 1 if inside the volume.
        bool ComputeStructuredCoordinates(Vector3 xyz, int[] ijk, ref Vector3 pcoords)
        {
            // tolerance is needed for 2D data (this is squared tolerance)
            const float tol2 = 1e-8f;
            //
            //  Compute the ijk location
            //
            int isInBounds = 1;
            Vector3 boundMin = this.dataset.origin;
            Vector3 boundMax = this.dataset.origin + this.dataset.size;
            Vector3 tmpCoord;

            for (int i = 0; i < 3; i++)
            {
                float d = xyz[i] - this.dataset.origin[i];
                float doubleLoc = d / this.dataset.spacing[i];
                // Floor for negative indexes.
                ijk[i] = Mathf.FloorToInt(doubleLoc);
                pcoords[i] = doubleLoc - (1.0f * (ijk[i]));

                int tmpInBounds = 0;
                //int minExt = extent[i * 2];
                //int maxExt = extent[i * 2 + 1];
                int minExt = 0;
                int maxExt = this.dataset.gridDim2[i] - 1;

                // check if data is one pixel thick
                if (minExt == maxExt)
                {
                    double dist = xyz[i] - boundMin[i];
                    if (dist * dist <= this.dataset.spacing[i] * this.dataset.spacing[i] * tol2)
                    {
                        pcoords[i] = 0.0f;
                        ijk[i] = minExt;
                        tmpInBounds = 1;
                    }
                }

                // low boundary check
                else if (ijk[i] < minExt)
                {
                    if ((this.dataset.spacing[i] >= 0 && xyz[i] >= boundMin[i]) ||
                        (this.dataset.spacing[i] < 0 && xyz[i] <= boundMax[i]))
                    {
                        pcoords[i] = 0.0f;
                        ijk[i] = minExt;
                        tmpInBounds = 1;
                    }
                }

                // high boundary check
                else if (ijk[i] >= maxExt)
                {
                    if ((this.dataset.spacing[i] >= 0 && xyz[i] <= boundMax[i]) ||
                        (this.dataset.spacing[i] < 0 && xyz[i] >= boundMin[i]))
                    {
                        // make sure index is within the allowed cell index range
                        pcoords[i] = 1.0f;
                        ijk[i] = maxExt - 1;
                        tmpInBounds = 1;
                    }
                }

                // else index is definitely within bounds
                else
                {
                    tmpInBounds = 1;
                }

                // clear isInBounds if out of bounds for this dimension
                isInBounds = (isInBounds & tmpInBounds);
            }

            tmpCoord = pcoords;
            pcoords[1] = tmpCoord[2];
            pcoords[2] = tmpCoord[1];

            if (isInBounds == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // GetCell should be a member of the ImageData class
        void GetCell(int cellId, ref VoxelCell cell)
        {
            int[] ijk = new int[3];
            int iMin, iMax, jMin, jMax, kMin, kMax;
            Vector3 origin = this.dataset.origin;
            Vector3 spacing = this.dataset.spacing;
            int[] dims = new int[3];
            dims[0] = this.dataset.gridDim2[0];
            dims[1] = this.dataset.gridDim2[1];
            dims[2] = this.dataset.gridDim2[2];

            // Megaflow assumes that extents all start at zero whereas vtk does not
            // this is vtk code so I am dummying up the extents so that the code only needs to be 
            // slightly modified when we can read in a vtk .vti file
            int[] extent = new int[6];
            extent[0] = extent[2] = extent[4] = 0;
            extent[1] = dims[0];
            extent[3] = dims[1];
            extent[5] = dims[2];

            if (dims[0] == 0 || dims[1] == 0 || dims[2] == 0)
            {
                cell.SetCellToEmpty();
                return;
            }
            cell.SetCellToFull();

            cell.GetCellIndices(ref ijk);
            int[] ids = new int[8];

            iMin = ijk[0];
            iMax = iMin + 1;
            jMin = ijk[1];
            jMax = jMin + 1;
            kMin = ijk[2];
            kMax = kMin + 1;

            // Extract point coordinates and point ids
            int idx, npts = 0;
            Vector3 xyz;
            int[] loc = new int[3];
            for (loc[2] = kMin; loc[2] <= kMax; loc[2]++)
            {
                xyz.z = origin.z + (loc[2] + extent[4]) * spacing.z;
                for (loc[1] = jMin; loc[1] <= jMax; loc[1]++)
                {
                    xyz.y = origin.y + (loc[1] + extent[2]) * spacing.y;
                    for (loc[0] = iMin; loc[0] <= iMax; loc[0]++)
                    {
                        xyz.x = origin.x + (loc[0] + extent[0]) * spacing.x;

                        // the following is the vtk code
                        //idx = loc[0] + (loc[1]*dims[0]) + (loc[2]*dims[0]*dims[1]);
                        // this is my code
                        idx = loc[0] + (loc[2] * dims[0]) + (loc[1] * dims[0] * dims[2]);

                        // set the cell point and ids
                        cell.SetPointData(npts, idx, xyz);
                        npts++;
                    }
                }
            }
        }
        /*
        public void SetCell(int cellId)
        {
            if (this.cell.GetCellId() != cellId)
            {

            }
        }
        */

        /*
                static bool CheckPointValidity(Vector3 vel)
                {
                    float tol = 0.000001f;
                    float value = Mathf.Abs(vel.x) + Mathf.Abs(vel.y) + Mathf.Abs(vel.z);

                    if (value < tol)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
*/

        public bool GetLastWeights(ref float[] w)
        {
            if (LastCellId < 0)
            {
                return false;
            }

            for (int i=0; i<8; i++)
            {
                w[i] = Weights[i];
            }
            return true;
        }

        float GetFieldDiagonal()
        {
            Vector3 dim = dataset.size;
            float sum = (dim.x * dim.x) + (dim.y * dim.y) + (dim.z * dim.z);

            return Mathf.Sqrt(sum);
        }

        public void ClearLastCellId()
        {
            this.LastCellId = -1;
        }

        bool CheckPCoords(Vector3 pcoords)
        {
            for (int i = 0; i < 3; i++)
            {
                if (pcoords[i] < 0 || pcoords[i] > 1)
                {
                    return false;
                }
            }

            return true;
        }

        // this should also be in the data set class
        Vector3 GetPointFromIndices(int[] ijk)
        {
            // get the coordinates for the point
            Vector3 p = Vector3.zero;
            p.z = this.dataset.origin.z + (ijk[2] * this.dataset.spacing.z);
            p.y = this.dataset.origin.y + (ijk[1] * this.dataset.spacing.y);
            p.x = this.dataset.origin.x + (ijk[0] * this.dataset.spacing.x);

            return p;
        }

        public void InterpolatePoint(float[] weights)
        {

        }

        public void SetNormalizeVector(bool normalize)
        {
            NormalizeVector = normalize;
        }
    }
}

