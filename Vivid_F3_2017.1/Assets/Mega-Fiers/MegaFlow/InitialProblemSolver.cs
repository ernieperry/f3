﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IVI_CFD
{
    public class InitialProblemSolver
    {
        public enum ErrorCodes
        {
            OUT_OF_DOMAIN = 1,
            NOT_INITIALIZED = 2,
            UNEXPECTED_VALUE = 3
        }

        public struct StepPoints
        {
            public Vector3 point;
            public float time;
        }

        public ImageDataSet dataset;
        public VelocityFieldInterpolator interpolator;
        public List<StepPoints> Vals = new List<StepPoints>();
        public List<Vector3> Derivs = new List<Vector3>();
        public bool Initialized;
        public bool Adaptive;
        public bool Modified;

        public InitialProblemSolver()
        {
            dataset = null;
            interpolator = null;
            Initialized = false;
            Adaptive = false;
            Modified = false;
        }

        public InitialProblemSolver(ImageDataSet functionSet, VelocityFieldInterpolator interp)
        {
            dataset = functionSet;
            interpolator = interp;
            Initialized = false;
            Adaptive = false;
            Modified = false;
        }

        ~InitialProblemSolver()
        {
            dataset = null;
            Vals.Clear();
            Derivs.Clear();
        }

        public void SetFunctionSet(ImageDataSet functionSet)
        {
            if (dataset != functionSet && functionSet != null)
            {
                dataset = functionSet;
                Modified = true;
            }
            Initialize();
        }

        public void SetInterpolator(VelocityFieldInterpolator interp)
        {
            interpolator = interp;
        }

        public bool IsAdaptive()
        {
            return Adaptive;
        }

        public void Initialize()
        {
            if (!dataset)
            {
                return;
            }
            Vals.Clear();
            Derivs.Clear();
            Initialized = true;
        }

    }
}
