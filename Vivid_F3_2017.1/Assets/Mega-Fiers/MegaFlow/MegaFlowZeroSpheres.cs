﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using IVI_CFD;

public class MegaFlowZeroSpheres : MonoBehaviour
{
    public MegaFlow source;

    ImageDataSet frame;
    public int framenum;

    public void SetFrame(int f)
    {
        if (source)
        {
            if (f >= 0 && f < source.frames.Count)
            {
                frame = source.frames[f];
                framenum = f;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
       DisplaySpheres(source);
    }

    // Update is called once per frame
    void Update()
    {
       // DisplaySpheres(source);
    }

    void DisplaySpheres(MegaFlow mod)
    {
        int xd = mod.flow.gridDim2[0];
        int yd = mod.flow.gridDim2[1];
        int zd = mod.flow.gridDim2[2];

        Vector3 origin = mod.flow.origin;

        //float adjx = mod.flow.spacing.x * 0.5f;
        //float adjy = mod.flow.spacing.y * 0.5f;
        //float adjz = mod.flow.spacing.z * 0.5f;

        //bool inbounds = false;

        //mod.Prepare();
        int cnt = 0;

        Vector3 p;

        for (int z = 0; z < zd; z+=3)
         {
            p.z = origin.z + (z * mod.flow.spacing.z);

            for (int y = 0; y < yd; y+=3)
            {
                p.y = origin.y + (y * mod.flow.spacing.y);

                for (int x = 0; x < xd; x+=5)
                //for (int x = 0; x < 15; x++)
                {
                    p.x = origin.x + (x * mod.flow.spacing.x);

                    int indx = x + (y * xd) + (z * yd * zd);
                    if (indx == 72404 || indx == 72504)
                    {
                        int dummy = 0;
                    }

                    Vector3 vel = mod.flow.SampleVelUnity(x, y, z);
                    cnt++;

                    if (vel.magnitude < 0.00001)
                    {
                        /*
                        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        cube.transform.position = p;
                        //cube.GetComponent<Renderer>().material.color = Color.blue;
                        cube.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
                        */

                        Vector3 incre = new Vector3(0.01f, 0.0f, 0.0f);
                        Vector3 p1 = p + incre;

                        GameObject ptline = new GameObject();
                        LineRenderer line = ptline.AddComponent<LineRenderer>();
                        line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                        line.receiveShadows = false;
                        //line.material = new Material(Shader.Find("Particles/Additive"));
                        //line.material = new Material(Shader.Find("Vertex Colors"));
                        //line.startColor = Color.blue;
                        //line.endColor = Color.blue;
                        line.startWidth = 0.01f;
                        line.endWidth = 0.01f;

                        Vector3[] positions = new Vector3[2];
                        positions[0] = p;
                        positions[1] = p1;
                        line.positionCount = 2;
                        line.SetPositions(positions);
                    }
                    else
                    {
                        int dummy = 0;
                    }
                   
                }
            }
        }
    }

}
      

