﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AvatarController : MonoBehaviour {

    private GameObject hmdObj;
    private GameObject avatar;

    // Use this for initialization
    void Start ()
    {
        avatar = this.gameObject;
        hmdObj = GameObject.FindGameObjectWithTag("MainCamera");

        if (avatar.GetComponent<NetworkIdentity>().isLocalPlayer)
        {
            avatar.SetActive(false);
        }
        else
        {
            avatar.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update ()
    {
        avatar.transform.position = hmdObj.transform.position;
        //avatar.transform.Translate( 0.0f, 0.15f, -0.05f);
        avatar.transform.rotation = hmdObj.transform.rotation;

    }
}
